package com.example.donotusethis.moodleplus;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by DO NOT USE THIS! on 2/21/2016.
 */
public class ThreadView extends ArrayAdapter<JSONObject> {

    /**
     * Constructor
     *
     * @param context  The current context.
     * @param objects  The objects to represent in the ListView.
     */
    public ThreadView(Context context, JSONObject[] objects) {
        super(context, R.layout.thrd_view, objects);
    }

    /**
     * {@inheritDoc}
     *
     * @param position
     * @param convertView
     * @param parent
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater myInflater= LayoutInflater.from(getContext());
        View myCustomview= myInflater.inflate(R.layout.thrd_view, parent, false);

        try{
            JSONObject singleItem= getItem(position);
            TextView textView = (TextView)myCustomview.findViewById(R.id.myThrdName);
            String title= singleItem.getString("title");
            textView.setText(title);
        }catch (JSONException e){
            Toast.makeText(getContext(), "JSONError", Toast.LENGTH_SHORT).show();
        }

        return myCustomview;
    }
}
