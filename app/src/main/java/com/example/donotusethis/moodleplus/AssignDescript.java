package com.example.donotusethis.moodleplus;

import android.content.Intent;
import android.os.Bundle;
import android.preference.TwoStatePreference;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

public class AssignDescript extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assign_descript);

        Bundle bundle=getIntent().getExtras();
        if (bundle!=null){
            int id=bundle.getInt("id");
            sendRequest(id);
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    private void sendRequest(int id){

        final TextView mHead=(TextView)findViewById(R.id.assgnHead);
        final TextView mDescript=(TextView)findViewById(R.id.assgnDescript);
        final TextView mCreated=(TextView)findViewById(R.id.created);
        final TextView mDeadline=(TextView)findViewById(R.id.deadline);
        final TextView mTimeLeft=(TextView)findViewById(R.id.difference);//lefthereyouasshole
        String url="http://192.168.23.1:8000/courses/assignment.json/"+String.valueOf(id);
        RequestQueue requestQueue= Volley.newRequestQueue(this);
        StringRequest stringRequest=new StringRequest(
                Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                       try{
                           JSONObject jsonObject=new JSONObject(response).getJSONObject("assignment");
                           mHead.setText(jsonObject.getString("name"));
                           mDescript.setText(Html.fromHtml(jsonObject.getString("description")));
                           try{
                               String s=java.net.URLDecoder.decode(jsonObject.getString("description"), "UTF-8");
                           }catch(UnsupportedEncodingException e){
                                mDescript.setText(Html.fromHtml(jsonObject.getString("description")));
                           }
                       }catch (JSONException e){
                           Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                       }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                },this
        );
        requestQueue.add(stringRequest);
    }
}
