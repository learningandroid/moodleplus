package com.example.donotusethis.moodleplus;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by DO NOT USE THIS! on 2/16/2016.
 */
public class Users extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION=1;
    private static final String DATABASE_NAME="storeUsers";
    private static final String TABLE_NAME="users";
    private static final String ID="ID";
    private static final String USER="USER";
    private static final String TABLE_CREATE= "CREATE TABLE "+TABLE_NAME+" ("+
            ID+" INTEGER PRIMARY KEY AUTOINCREMENT, "+USER+
            " TEXT);";

    public Users(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void addUser(String username){
        
    }
}
