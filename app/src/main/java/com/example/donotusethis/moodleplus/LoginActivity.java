package com.example.donotusethis.moodleplus;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Displays a login screen
 * and executes basic button commands
 * to move to next activity as approriate.
 */
public class LoginActivity extends AppCompatActivity {

    /**
     * @mEmailView for autocomplete username
     * @mPasswordView for password
     * @mProgressView to show password at start
     * @instance login instance
     * @PREFS for preferences
     */
    // UI references.
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;

    public static LoginActivity instance;

    public static final String PREFS="MyPrefs";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        /**
         * restores login session
         * retrieves username if stored
         * gives most used username suggestions
         * can click even editor for logging in
         * splash screen appears
         */
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        instance=this;

        //Restore preferences
        SharedPreferences sharedPreferences=getSharedPreferences(PREFS, Context.MODE_PRIVATE);
        String cookie=sharedPreferences.getString(StringRequest.SESSION_COOKIE, null);
        if (cookie!=null){

            Intent intent=new Intent(this, MyCourses.class);
            startActivity(intent);
        }

        String remember= sharedPreferences.getString("remeberMe", "");
        long current=System.currentTimeMillis();
        long expire= sharedPreferences.getLong("expire", current);
        if (expire>current)
            mEmailView.setText(remember);

        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);
        populateAutoComplete();

        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);

        final Handler handler=new Handler(){
            @Override
            public void handleMessage(Message msg) {
                moveTop((TextView)findViewById(R.id.splashText));
                findViewById(R.id.login_form).setVisibility(View.VISIBLE);
                View progress=findViewById(R.id.login_progress);
                ((ViewManager)progress.getParent()).removeView(progress);
                openKeyboard(instance, mEmailView);
            }
        };

        Runnable runnable = new Runnable() {
            public void run() {
                long futureTime=System.currentTimeMillis()+1000;
                while (System.currentTimeMillis()<futureTime){
                    synchronized (this){
                        try {
                            wait(futureTime-System.currentTimeMillis());
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
                handler.sendEmptyMessage(0);
            }
        };
        Thread thread= new Thread(runnable);
        thread.start();
    }

    public static void openKeyboard(Activity activity, View editText){//send focus to and open keyboard
        editText.setFocusableInTouchMode(true);
        editText.requestFocus();
        ((InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE)).showSoftInput(
                editText, InputMethodManager.SHOW_IMPLICIT);
    }

    public void moveTop(TextView textView){

        RelativeLayout.LayoutParams textPosition = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT
        );
        textPosition.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
        textView.setLayoutParams(textPosition);
    }

    private void populateAutoComplete() {

        /*String[] listIds=new String[5];
        byte[] buffer=new byte[50];
        try {
            FileInputStream fis=openFileInput("hello.txt");
            fis.read(buffer,0,50);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String str=buffer.toString();
        String[] listIds1=str.split("\n");

        for(int i=0;i<listIds1.length&&i<5;i++){
            listIds[i]=listIds1[i];
        }
        ArrayAdapter<String> adapter=new ArrayAdapter<String>(
                this,R.layout.support_simple_spinner_dropdown_item,0,listIds);
        mEmailView.setAdapter(adapter);*/
    }



    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {

        /*try {
            FileOutputStream fos=openFileOutput("hello.txt",2);
            fos.write((mEmailView.getText().toString() + "\n").getBytes());
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }*/

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        String error=null;

        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(password)) {
            mPasswordView.setError(getString(R.string.error_field_required));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        }else if ((error= isEmailValid(email))!=null){
            mEmailView.setError(error);
            focusView=mEmailView;
            cancel=true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            sendRequest(email, password);
        }
    }

    private void sendRequest(final String mEmail, final String mPassword) {
        /**
         * sends request to login to the system using get request
         * a custom request used to store cookies
         */

        String url="http://192.168.23.1:8000/default/login.json?userid="+mEmail+"&password="+mPassword;
        RequestQueue requestQueue= Volley.newRequestQueue(instance);
        StringRequest stringRequest=new StringRequest(
                Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try{
                            JSONObject jsonObject=new JSONObject(response);
                            boolean success= jsonObject.getBoolean("success");
                            if (success){
                                JSONObject user=jsonObject.getJSONObject("user");
                                SharedPreferences.Editor editor=getSharedPreferences(PREFS, 0).edit();
                                editor.putInt("type", user.getInt("type_"));
                                editor.putString("username", user.getString("username"));
                                CheckBox checkBox=(CheckBox)findViewById(R.id.myCheckBox);
                                if (checkBox.isChecked()){
                                    editor.putString("remeberMe", mEmail);
                                    editor.putLong("expire", System.currentTimeMillis() + 2592000 * 1000);
                                }
                                editor.commit();
                                Intent intent= new Intent(instance, MyCourses.class);
                                startActivity(intent);
                            }else {
                                Toast.makeText(instance, "Invalid Login", Toast.LENGTH_SHORT).show();
                                mEmailView.requestFocus();
                            }
                        }catch(JSONException e){
                            Toast.makeText(instance, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(instance, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                },this
        );
        requestQueue.add(stringRequest);
    }

    private String isEmailValid(String email) {

        /**
         * validates email
         */
        //TODO: Replace this with your own logic
        return null;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }
}

