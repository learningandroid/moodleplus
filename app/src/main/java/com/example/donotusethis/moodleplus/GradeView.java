package com.example.donotusethis.moodleplus;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by DO NOT USE THIS! on 2/21/2016.
 */
public class GradeView extends ArrayAdapter<JSONObject> {

    /**
     * Constructor
     *
     * @param context  The current context.
     * @param objects  The objects to represent in the ListView.
     */
    public GradeView(Context context, JSONObject[] objects) {
        super(context, R.layout.grd_view, objects);
    }

    /**
     * {@inheritDoc}
     *
     * @param position
     * @param convertView
     * @param parent
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater myInflater= LayoutInflater.from(getContext());
        View myCustomview= myInflater.inflate(R.layout.grd_view, parent, false);

        try{
            JSONObject singleItem= getItem(position);
            TextView name = (TextView)myCustomview.findViewById(R.id.grade_item);
            TextView score=(TextView)myCustomview.findViewById(R.id.grade_score);
            TextView weight=(TextView)myCustomview.findViewById(R.id.grade_weight);
            TextView marks= (TextView)myCustomview.findViewById(R.id.grade_marks);
            int out_of_s= singleItem.getInt("out_of");
            int score_s= singleItem.getInt("score");
            name.setText(singleItem.getString("name"));
            score.setText(String.valueOf(score_s)+"/"+String.valueOf(out_of_s));
            int weight_s=singleItem.getInt("weightage");
            weight.setText(String.valueOf(weight_s));
            float marks_s= ((float)score_s)/((float)out_of_s);
            marks_s=Math.round((double)marks_s*((float)weight_s)*100)/100;
            marks.setText(String.valueOf(marks_s));
        }catch (JSONException e){
            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }

        return myCustomview;
    }
}
