package com.example.donotusethis.moodleplus;

/**
 * Created by DO NOT USE THIS! on 2/17/2016.
 */
public class MyCourse {

    String code, name, description, l_t_p;
    int credits, id;

    public MyCourse(String code, String name, String l_t_p, int id, String description, int credits) {
        this.code = code;
        this.name = name;
        this.l_t_p = l_t_p;
        this.id = id;
        this.description = description;
        this.credits = credits;
    }
}
