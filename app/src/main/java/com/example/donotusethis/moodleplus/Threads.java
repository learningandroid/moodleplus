package com.example.donotusethis.moodleplus;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by DO NOT USE THIS! on 2/17/2016.
 */
public class Threads extends Fragment {

    EditText myTitle, myDescription;

    public Threads() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view=inflater.inflate(R.layout.frg_threads, container, false);

        final EachCourse activity=(EachCourse)getActivity();

        String response=activity.threads;
        if (response==null)
            response="";
        try{

            JSONObject jsonObject=new JSONObject(response);
            JSONArray jsonArray=jsonObject.getJSONArray("course_threads");
            int len=jsonArray.length();
            final JSONObject[] assgnDetails=new JSONObject[len];
            for (int i=0;i<len;i++)
                assgnDetails[i]=jsonArray.getJSONObject(i);
            ListAdapter listAdapter=new ThreadView(activity, assgnDetails);
            ListView listView=(ListView)view.findViewById(R.id.thrd_list);
            listView.setAdapter(listAdapter);
            listView.setOnItemClickListener(
                    new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Intent intent =new Intent(getContext(), ThreadDescript.class);
                            try{
                                intent.putExtra("id", assgnDetails[position].getInt("id"));
                            }catch (JSONException e){
                                intent.putExtra("id", -1);
                            }
                            startActivity(intent);
                        }
                    }
            );

        }catch (JSONException e){
//            Toast.makeText(activity, e.getMessage(), Toast.LENGTH_SHORT).show();
        }

        view.findViewById(R.id.thrd_submit).setOnClickListener(
                new View.OnClickListener() {
                    /**
                     * Called when a view has been clicked.
                     *
                     * @param v The view that was clicked.
                     */
                    @Override
                    public void onClick(View v) {
                        myTitle = (EditText) view.findViewById(R.id.myThreadTitle);
                        myDescription = (EditText) view.findViewById(R.id.myThreadDescription);

                        postThread(activity.code,
                                toAscii(myTitle.getText().toString()),
                                toAscii(myDescription.getText().toString()));

                    }
                }
        );

        return view;
    }

    public static String toAscii(String input){
        int len=input.length();
        StringBuilder builder=new StringBuilder();
        for(int i=0;i<len;i++){
            builder.append("%");
            builder.append(Integer.toHexString((int)input.charAt(i)));
        }
        return builder.toString();
    }

    private void postThread(final String code, final String title, String description){
        final Context context =getContext();
        String url="http://192.168.23.1:8000/threads/new.json?title="
                +title+"&description="+description+"&course_code="+code;
        RequestQueue requestQueue= Volley.newRequestQueue(context);
        StringRequest stringRequest=new StringRequest(
                Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try{
                            boolean success=new JSONObject(response).getBoolean("success");
                            if (!success)
                                Toast.makeText(context, "Thread could not be posted", Toast.LENGTH_SHORT).show();
                            else{

                                ((EachCourse)context).loadAll(true);
                            }
                        }catch(JSONException e){
                            Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                },(Activity)context
        );
        requestQueue.add(stringRequest);
    }
}
