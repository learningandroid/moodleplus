package com.example.donotusethis.moodleplus;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by DO NOT USE THIS! on 2/17/2016.
 */
public class Assignments extends Fragment {

    public Assignments() {

    }



    /**
     * Called to do initial creation of a fragment.  This is called after
     * {@link #onAttach(Activity)} and before
     * {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}.
     * <p>
     * <p>Note that this can be called while the fragment's activity is
     * still in the process of being created.  As such, you can not rely
     * on things like the activity's content view hierarchy being initialized
     * at this point.  If you want to do work once the activity itself is
     * created, see {@link #onActivityCreated(Bundle)}.
     *
     * @param savedInstanceState If the fragment is being re-created from
     *                           a previous saved state, this is the state.
     */
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.frg_assignments, container, false);

        final EachCourse activity=(EachCourse)getActivity();

        String response=activity.assignments;
        if (response==null)
            response="";
        try{

            final JSONObject jsonObject=new JSONObject(response);
            JSONArray jsonArray=jsonObject.getJSONArray("assignments");
            int len=jsonArray.length();
            final JSONObject[] assgnDetails=new JSONObject[len];
            for (int i=0;i<len;i++)
                assgnDetails[i]=jsonArray.getJSONObject(i);
            ListAdapter listAdapter=new AssgnView(activity, assgnDetails);
            ListView listView=(ListView)view.findViewById(R.id.assignList);
            listView.setAdapter(listAdapter);
            listView.setOnItemClickListener(
                    new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Intent intent=new Intent(getContext(), AssignDescript.class);
                            try{
                                intent.putExtra("id", assgnDetails[position].getInt("id"));
                            }catch (JSONException e){
                                intent.putExtra("id", -1);
                            }
                            startActivity(intent);
                        }
                    }
            );
        }catch (JSONException e){
//            Toast.makeText(activity, e.getMessage(), Toast.LENGTH_SHORT).show();
        }

        return view;
    }
}
