package com.example.donotusethis.moodleplus;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by DO NOT USE THIS! on 2/21/2016.
 */
public class CmntView extends ArrayAdapter<JSONObject> {

    /**
     * Constructor
     *
     * @param context  The current context.
     * @param objects  The objects to represent in the ListView.
     */
    public CmntView(Context context, JSONObject[] objects) {
        super(context, R.layout.cmnt_list_view, objects);
    }

    /**
     * {@inheritDoc}
     *
     * @param position
     * @param convertView
     * @param parent
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater myInflater= LayoutInflater.from(getContext());
        View myCustomview= myInflater.inflate(R.layout.cmnt_list_view, parent, false);

        JSONObject singleItem= getItem(position);
        TextView cmntTxt=(TextView)myCustomview.findViewById(R.id.cmnt_text);
        try{
            cmntTxt.setText(singleItem.getString("description"));
        }catch(JSONException e){

        }

        return myCustomview;
    }
}
