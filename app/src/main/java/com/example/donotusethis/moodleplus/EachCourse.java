package com.example.donotusethis.moodleplus;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import java.util.ArrayList;
import java.util.List;

public class EachCourse extends AppCompatActivity {

    public String assignments, threads, grades, code;

    ViewPager viewPager;
    TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_each_course);

        Bundle extras=getIntent().getExtras();
        if (extras!=null){
            code=extras.getString("code");
            ((TextView)findViewById(R.id.each_course_title)).setText(code);
            loadAll(false);
        }else{
            Toast.makeText(this, "Intent Error", Toast.LENGTH_SHORT).show();
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        viewPager= (ViewPager)findViewById(R.id.pager);
        tabLayout=(TabLayout)findViewById(R.id.tabs);
    }

    public void viewCreate(boolean b){
        ViewPagerAdapter adapter= new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new Assignments(), "Assignments");
        viewPager.setAdapter(adapter);
        adapter.addFragment(new Threads(), "Threads");
        viewPager.setAdapter(adapter);
        if (b)
            viewPager.setCurrentItem(1);
        SharedPreferences preferences = getSharedPreferences(LoginActivity.PREFS, 0);
        if(preferences.getInt("type", 0)==0){
            adapter.addFragment(new Grades(), "Grades");
            viewPager.setAdapter(adapter);
        }
        tabLayout.setupWithViewPager(viewPager);
    }

    public void loadAll(final boolean b){

        final EachCourse eachCourse=this;
        final String url= "http://192.168.23.1:8000/courses/course.json/"+code;
        RequestQueue requestQueue= Volley.newRequestQueue(this);
        StringRequest assign=new StringRequest(
                Request.Method.GET, url+"/assignments",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        assignments=response;
                        viewCreate(b);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(eachCourse, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                },this
        );
        final StringRequest thrds=new StringRequest(
                Request.Method.GET, url+"/threads",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        threads=response;
                        viewCreate(b);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(eachCourse, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                },this
        );
        StringRequest grds=new StringRequest(
                Request.Method.GET, url+"/grades",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        grades=response;
                        viewCreate(b);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(eachCourse, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                },this
        );
        requestQueue.add(assign);
        requestQueue.add(thrds);
        requestQueue.add(grds);
    }

    /*private void setupViewPager(ViewPager viewPager) {

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new Overview(), "Overview");
        *//*Assignments afragment=new Assignments();
        Bundle bundle=new Bundle();
        bundle.putString("response", assignments);
        afragment.setArguments(bundle);*//*
        adapter.addFragment(new Assignments(), "Assignments");
       *//* adapter.addFragment(new Resource(), "Resources");
        Threads tfragment=new Threads();
        bundle=new Bundle();
        bundle.putString("response", threads);
        tfragment.setArguments(bundle);*//*
        adapter.addFragment(new Threads(), "Threads");
        *//*Grades gfragment=new Grades();
        bundle=new Bundle();
        bundle.putString("response", grades);
        gfragment.setArguments(bundle);*//*
        adapter.addFragment(new Grades(), "Grades");
        viewPager.setAdapter(adapter);
    }*/

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_each_course, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            MyCourses.logOut();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
