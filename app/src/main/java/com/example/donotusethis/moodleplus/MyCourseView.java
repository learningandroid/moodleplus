package com.example.donotusethis.moodleplus;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by DO NOT USE THIS! on 2/17/2016.
 */
public class MyCourseView extends ArrayAdapter<JSONObject> {

    public MyCourseView(Context context, JSONObject[] courses) {
        super(context, R.layout.course_view, courses);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater myInflater= LayoutInflater.from(getContext());
        View myCustomview= myInflater.inflate(R.layout.course_view, parent, false);

        JSONObject singleItem= getItem(position);
        TextView text_name = (TextView)myCustomview.findViewById(R.id.myCourseName);
        TextView text_code= (TextView)myCustomview.findViewById(R.id.my_course_code);
        TextView text_ltp=(TextView)myCustomview.findViewById(R.id.my_course_ltp);
        try{
            text_name.setText(singleItem.getString("name"));
            final String code=singleItem.getString("code");
            text_code.setText(code);
            myCustomview.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent=new Intent(MyCourses.courses, EachCourse.class);
                            intent.putExtra("code", code);
                            MyCourses.courses.startActivity(intent);
                        }
                    }
            );
            text_ltp.setText(singleItem.getString("l_t_p"));
        }catch (JSONException e){
            Toast.makeText(MyCourses.courses, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            myCustomview.setElevation(10);
        myCustomview.setBackgroundColor(Color.parseColor("#205050"));
        myCustomview.setPadding(5, 5, 5, 5);
        return myCustomview;
    }
}
