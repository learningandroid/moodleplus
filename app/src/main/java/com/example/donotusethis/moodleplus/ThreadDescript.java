package com.example.donotusethis.moodleplus;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

public class ThreadDescript extends AppCompatActivity {

    int id;
    EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thread_descript);


        editText=(EditText)findViewById(R.id.thrdComment);
        Bundle bundle=getIntent().getExtras();
        if (bundle!=null){
            id=bundle.getInt("id");
            sendRequest(id);
        }
        Button addComment=(Button)findViewById(R.id.AddCmnt);
        addComment.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        sendComment(Threads.toAscii(editText.getText().toString()));
                    }
                }
        );

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    private void sendComment(String comment){
        String url="http://192.168.23.1:8000/threads/post_comment.json?thread_id="+String.valueOf(id)
                +"&description="+comment;
        RequestQueue requestQueue= Volley.newRequestQueue(this);
        StringRequest stringRequest=new StringRequest(
                Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try{
                           if ((new JSONObject(response)).getBoolean("success"))
                               sendRequest(id);
                           else
                               Toast.makeText(getApplicationContext(),
                                       "Unable to send comment", Toast.LENGTH_SHORT).show();
                        }catch (JSONException e){
                            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                },this
        );
        requestQueue.add(stringRequest);
    }

    private void sendRequest(int id){

        editText.setText("");
        final TextView mHead=(TextView)findViewById(R.id.thrdTitle);
        final TextView mDescript=(TextView)findViewById(R.id.thrdDescript);
        final ListView mCommentList =(ListView)findViewById(R.id.cmntList);
        String url="http://192.168.23.1:8000/threads/thread.json/"+String.valueOf(id);
        RequestQueue requestQueue= Volley.newRequestQueue(this);
        StringRequest stringRequest=new StringRequest(
                Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try{
                            JSONObject jsonObject=new JSONObject(response);
                            JSONObject jsonThread=jsonObject.getJSONObject("thread");
                            mHead.setText(jsonThread.getString("title"));
                            mDescript.setText(jsonThread.getString("description"));
                            JSONArray cmnts= jsonObject.getJSONArray("comments");
                            int len=cmnts.length();
                            JSONObject[] jArray=new JSONObject[len];
                            for (int i=0;i<len;i++){
                                jArray[i]=cmnts.getJSONObject(i);
                            }
                            ListAdapter adapter=new CmntView(getApplicationContext(), jArray);
                            mCommentList.setAdapter(adapter);
                        }catch (JSONException e){
                            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                },this
        );
        requestQueue.add(stringRequest);
    }
}
