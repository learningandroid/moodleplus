package com.example.donotusethis.moodleplus;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by DO NOT USE THIS! on 2/17/2016.
 */
public class Grades extends Fragment {


    public Grades() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.frg_grades, container, false);

        final EachCourse activity=(EachCourse)getActivity();


        String response=activity.grades;
        if (response==null)
            response="";
        try{
            int  totalWeight=0;
            float totalMarks=0;
            JSONObject jsonObject=new JSONObject(response);
            JSONArray jsonArray=jsonObject.getJSONArray("grades");
            int len=jsonArray.length();
            JSONObject[] assignDetails=new JSONObject[len];
            for (int i=0;i<len;i++){
                JSONObject jobj=jsonArray.getJSONObject(i);
                int out_of_s= jobj.getInt("out_of");
                int score_s= jobj.getInt("score");
                int weight_s=jobj.getInt("weightage");
                float marks_s= ((float)score_s)/((float)out_of_s);
                marks_s=Math.round((double)marks_s*((float)weight_s)*100)/100;
                totalWeight+=weight_s;
                totalMarks+=marks_s;
                assignDetails[i]=jobj;
            }
            ((TextView)view.findViewById(R.id.out_of)).setText(
                    String.valueOf(totalMarks)+"/"+String.valueOf(totalWeight));
            ((TextView)view.findViewById(R.id.Total)).setText("Total");
            ((TextView)view.findViewById(R.id.total_weight)).setText(String.valueOf(totalWeight));
            ((TextView)view.findViewById(R.id.total_score)).setText(String.valueOf(totalMarks));
            ListView listView=(ListView)view.findViewById(R.id.grdList);
            ListAdapter adapter=new GradeView(activity, assignDetails);
            listView.setAdapter(adapter);
        }catch (JSONException e){
//            Toast.makeText(activity, e.getMessage(), Toast.LENGTH_SHORT).show();
        }

        return view;
    }
}
