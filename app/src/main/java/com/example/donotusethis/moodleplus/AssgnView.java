package com.example.donotusethis.moodleplus;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by DO NOT USE THIS! on 2/20/2016.
 */
public class AssgnView extends ArrayAdapter<JSONObject>{

    /**
     * Constructor
     *
     * @param context  The current context.
     */
    public AssgnView(Context context, JSONObject[] details) {
        super(context, R.layout.assgn_view,details);
    }

    /**
     * {@inheritDoc}
     *
     * @param position
     * @param convertView
     * @param parent
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater myInflater= LayoutInflater.from(getContext());
        View myCustomview= myInflater.inflate(R.layout.assgn_view, parent, false);

        try{
            JSONObject singleItem= getItem(position);
            TextView Aname = (TextView)myCustomview.findViewById(R.id.myAssgnName);
            TextView Atime = (TextView)myCustomview.findViewById(R.id.timeLeft);
            TextView description = (TextView)myCustomview.findViewById(R.id.shortDescript);

            String name= singleItem.getString("name");
            String[] sname= name.split(": ");
            String time=singleItem.getString("deadline");
            String descript=singleItem.getString("description").substring(0, 50);
            descript=descript+("...");

            Aname.setText(sname[1]);
            Atime.setText(getDifference(time));
            description.setText(Html.fromHtml(descript));
        }catch (JSONException e){
            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }

        return myCustomview;
    }

    public String getDifference(String deadline){
        SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
        DateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date d1=null;
        Date d2=new Date();

        try{
            d1=format.parse(deadline);
            format.format(d2);
//            Toast.makeText(getContext(), dateformat.format(d2)+d1, Toast.LENGTH_SHORT).show();
            //d2=format.parse(String.valueOf(new Date()));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        long diff=d1.getTime()-d2.getTime();
        String timeLeft="";
        if (diff<0){diff=-diff;
            timeLeft=" past deadline"+timeLeft;
         }

        long DiffSec=diff/1000%60;
        long DiffMin=diff/(60*1000)%60;
        long DiffHrs=diff/(60*60*1000)%24;
        long DiffDays=diff/(60*60*1000*24);
         if(DiffDays!=0)
              timeLeft=DiffDays+" days "+DiffHrs+":"+DiffMin+":"+DiffSec+timeLeft;
        else
            timeLeft=DiffHrs+":"+DiffMin+":"+DiffSec+timeLeft;
        return timeLeft;
    }
}
